---
title: "Microsoft password reset scam"
date: 2023-04-25T23:51:04+02:00
draft: true
---

I honestly had enough of people getting scammed to such a simple and basic scam. I think it's time for me to actually clarify how it works so you all don't all become scared to click on any link which probably wouldn't help always as some people started using discord bots to do the same thing.

## How??

The way this works is actually quite simple. The scammer would ask for your email or they may specifically say "minecraft account email" and at that point most people lower their guard and enter their email. Then after that they would thru some means send a password reset link to said email and ask for the code. To give some credit MS doesn't tell you what the code is for so I see why people end up sending it to the scammer but I digress. After you give them the code, well, you are kind of screwed. After that you are at the mercy of MS support, which is sort for you are most likely not getting your account tho you can try to.

## Ways to not get scammed

First download a brain you will need one. Second if you are required to enter and email to verify, especially if it says "minecraft account email" then that should be the first red flag. Good tip here is to provide an email you own that is not tied to a MS account and if you don't receive a code or the site/bot says invalid email then it's 99% a scam. Also please don't send anyone a code you revive in an email or sms. I feel like that is an old rule people knew.

## Good advice

If you encounter one of these sites you could do a lot by reporting the domain to [Google Safe Browsing](https://safebrowsing.google.com/safebrowsing/report_general/) and [Microsoft Report Unsafe Site](https://www.microsoft.com/en-us/wdsi/support/report-unsafe-site) which should have effect on 99.99% of users when it's reported. I highly encourage people to report these sites.
In case of discord bots you can go to [Discord Trust & Safety](https://support.discord.com/hc/en-us/requests/new?ticket_form_id=360000029731).