# Contact

## If you wish to contact me you can do so thru the means below

{{< rawhtml >}}
    <i class="fa fa-envelope" aria-hidden="true"></i>
{{< /rawhtml >}}
[Email](mailto:taken@mairimashita.org)
{{< rawhtml >}}
    </br></br><i class="fa fa-mastodon" aria-hidden="true"></i>
{{< /rawhtml >}}
[Mastadon](https://ani.work/@taken)
{{< rawhtml >}}
    </br></br><i class="fa fa-twitter" aria-hidden="true"></i>
{{< /rawhtml >}}
[Twitter](https://twitter.com/ignTakie)